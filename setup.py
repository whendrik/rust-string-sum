import sys

from setuptools import setup
from setuptools_rust import Binding, RustExtension

setup(
    # Needed to silence warnings (and to be a worthwhile package)
    name='rust-string-sum',
    version="1.0",
    url='https://gitlab.com/whendrik/my_rust_stringsum',
    author='Willem',
    author_email='whendrik@gmail.com',
    # Needed to actually package something
    packages=['rust_string_sum'],
    zip_safe=False,
    rust_extensions=[RustExtension("rust_string_sum.rust_string_sum", binding=Binding.PyO3)],
    include_package_data=True,
    description='An example of a python package from pre-existing code',
)