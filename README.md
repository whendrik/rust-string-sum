# Example to create a python package, backed by rust code

To benefit the speed & safety of [rust](https://www.rust-lang.org/), combined with the ease of use of python, you can create python packages that are written in rust.

In this repo, we re-create the [string-sum](https://github.com/PyO3/pyo3#using-rust-from-python) example of [pyO3](https://github.com/PyO3/pyo3), using the package/file structure of [html-py-ever](https://github.com/PyO3/setuptools-rust/tree/main/examples/html-py-ever).


## Installation of requirements and package

Steps needed to install this package in CP4D;

1. Install setuptools_rust, `!pip install setuptools_rust`
2. Install [rust](https://www.rust-lang.org/), `!curl https://sh.rustup.rs -sSf | sh -s -- --default-toolchain stable -y`
3. Install with pip from `git` with `!source $HOME/.cargo/env && pip install git+https://gitlab.com/whendrik/rust-string-sum.git#egg=rust-string-sum`

## Example of using the package

After install, use with 

```
import rust_string_sum

rust_string_sum.sum_as_string(3,4)
```

Which should output the `string` `"7"`.

## Example notebook with the above test

[Example.ipynb](/notebooks/Example.ipynb) contains a example notebook.